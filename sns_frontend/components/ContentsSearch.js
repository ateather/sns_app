import React from 'react';
import { Dropdown } from 'react-native-material-dropdown';
import { TextField } from 'react-native-material-textfield';
import  DatePicker  from 'react-native-datepicker';
import { TextButton, RaisedTextButton } from 'react-native-material-buttons';
import { MapView, ImagePicker, WebBrowser } from 'expo';
import axios from 'axios';
import Paths from '../constants/Paths';
import CategorySelector from './CategorySelector'

import PropTypes from 'prop-types';

import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  Switch,
  TouchableOpacity,
  View,
  Button, // for upload image
} from 'react-native';

export default class ContentsSearch extends React.Component {
    constructor(props) {
        super(props)
    }

    state = {
      title: '',
      message: '',
      choosingCategories: false,
      subbedTo: [],
      viewingCatagory: 'root',
      region: {
        latitude: 38.9897,
        longitude: -76.9378,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    }

    static propTypes = {
        onBack : PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
    }

    componentDidMount () {
        console.log("mounted")
    }

    _onBack () {
      this.props.onBack()
    }

    _onSubmit () {
      this.props.onSubmit(this.state.title,
         this.state.message, 
         this.state.subbedTo, 
         this.state.region.latitude,
         this.state.region.longitude)
    }

    _doneSubscribing () {
      console.log("Categories Subscribed")
      this.setState({
        viewingCategory: 'root',
        choosingCategories: false
      })
    }
  
    _subscriptionChange (cid, value) {
      let subbedTo = this.state.subbedTo
      console.log("[" + cid + "] => " + value)
      if (value)
        subbedTo.push(cid)
      else {
        let i = subbedTo.indexOf(cid)
        if (i >= 0)
          subbedTo.splice(i, 1)
      }
    }

    render ( ) {
      let {message} = this.state
      let {title} = this.state
      if (this.state.choosingCategories) {
        return (
          
            <CategorySelector
              parent = {this.state.viewingCatagory}
              onComplete = {this._doneSubscribing.bind(this)}
              onSubscribe = {this._subscriptionChange.bind(this)}
              subbedTo = {this.state.subbedTo}
            />
        )
      } else
        return (
          <ScrollView style={{flex:1}}>
            <View stretch style={styles.getMessagePart}>
              {/* Title of the Message */}
              <TextField
                label='Title' value={title}
                onChangeText={ (title) => this.setState({ title }) }
                containerStyle={styles.messageTitleStyle}
              />

              { /* Contents of the Message */}
              <TextField
                label='Message' value={message}
                onChangeText={ (message) => this.setState({ message }) }
                containerStyle={styles.mainMessageStyle}
                multiline={true}
              />
            </View>

            <MapView
              style={styles.map}
              initialRegion={{
                latitude: 38.9897,
                longitude: -76.9378,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
              onRegionChange={(region) => this.setState({region})}
            >
            <MapView.Marker
              coordinate={
                {latitude: this.state.region.latitude,
                longitude: this.state.region.longitude,}
              }
              title={"HERE!!"}
              description={"I am HERE!!"}
            />

            </MapView>

            <View style={{padding: 20, paddingVertical: 50}}>
              <RaisedTextButton
                title = "Choose Categories"
                onPress = {() => this.setState({choosingCategories: true})}
              >
              </RaisedTextButton>
            </View>

            <View style={{flex: 1, flexDirection: 'row', padding: 20}}>
              {/*Controlls go here */}
              <View style={{flex: 1, padding: 20 }}>
                <RaisedTextButton
                    title='Submit'
                    onPress={this._onSubmit.bind(this)}
                />
              </View>
              <View style={{flex: 1, padding: 20}}>
                <RaisedTextButton
                    title='Back'
                    onPress={this._onBack.bind(this)}
                />
              </View>
            </View>

            

          </ScrollView>
        )
    }
}


const styles = StyleSheet.create({
    messageStyle: {
      flex: 2,
      marginLeft: 10,
      marginRight: 10,
    },
    map: {
      height: 300,  /* The height is 400 pixels */
      width: '100%',  /* The width is the width of the web page */
      padding: 20
    },
    messageTitleStyle: {
      flex: 1,
      marginHorizontal: 0,
    },
    mainMessageStyle: {
      flex: 2,
      marginLeft: 10,
    },

    getMessagePart: {
      alignItems: 'center',
      flexDirection: 'row',
      marginHorizontal: 10,
    },


    container: {
      flex: 1,
      flexDirection: 'row',
      paddingTop: 15,
      backgroundColor: '#fff',
    },
    padding: {
      paddingHorizontal: 15,
      paddingVertical: 15
    },
    contentContainer: {
      paddingTop: 30,
    },

    //Doesn't WORK!!!!
    floatingContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      maxHeight: 220,
      ...Platform.select({
        ios: {
          shadowColor: 'black',
          shadowOffset: { height: -3 },
          shadowOpacity: 0.1,
          shadowRadius: 3,
        },
        android: {
          elevation: 20,
        },
      }),

      backgroundColor: '#fbfbfb',
      paddingVertical: 20,
    },

  });

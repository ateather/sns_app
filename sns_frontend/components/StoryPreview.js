import React from 'react'
import PropTypes from 'prop-types'
import { Text, View, Image } from 'react-native';
import { TextButton} from 'react-native-material-buttons';



export default class StoryPreview extends React.Component {
    static propTypes = {
        title : PropTypes.string,
        icon : PropTypes.string,
        onSelect: PropTypes.func,
        id : PropTypes.string
    }

    handleClick() {
        // navigate to this.props.id
        console.log("Navigating to Story[" + this.props.id + "]")
        //this.navigation.navigate("ViewStory.vue", {title: "Meme"})
        this.props.onSelect(this.props.id)
    }
    
    render() {
        return (
            <View data-id= {this.props.id} style= {{flex: 1, flexDirection: 'row'}}>
                <View style= {{width: 50}}>
                    <Image
                        style = {{width: 50, height: 50}}
                        source = {{uri: this.props.icon}}
                    />
                </View>
                

                <View style= {{
                    width:200, 
                    paddingHorizontal: 5, 
                    paddingVertical: 15
                }}>
                    <Text>{this.props.title}</Text>
                </View>

                <View style= {{width:50, paddingVertical: 10}}>
                    <TextButton
                        color= 'rgb(220,220,220)'
                        title= '->'
                        onPress= {this.handleClick.bind(this)}
                    />
                </View>
            </View>
        )
    }
}
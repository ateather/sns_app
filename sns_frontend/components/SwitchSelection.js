import React from 'react'
import {View, Text, FlatList, Switch, StyleSheet, Platform, TouchableOpacity} from 'react-native'
import PropTypes from 'prop-types'

export default class SwitchSelection extends React.Component {
    constructor(props) {
        super(props)

        let t = (props.value == true)
        this.state = {
            value : t
        }
    }

    static proptypes = {
        value : PropTypes.bool.isRequired,
        title : PropTypes.string.isRequired,
        onToggle : PropTypes.func,
        onSelect : PropTypes.func.isRequired
    }

    _onSubscribe = (value) => {
        this.setState({value : value})
        this.props.onToggle(this.props.title, value)
    }

    _onSelect = () => {
        this.props.onSelect(this.props.title)
    }

    render () {
        return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.textSpacing} onPress={this._onSelect.bind(this)}>
                <Text style={styles.text}>{this.props.title}</Text>
            </TouchableOpacity>
            <View style={styles.rightGroup}>
                <Switch 
                    value = {this.state.value}
                    onValueChange = {this._onSubscribe.bind(this)}/>
            </View>
        </View>

        )}
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      maxHeight: 80,
      paddingVertical: 10,
      backgroundColor: '#fff',
    },
    rightGroup: {
        width: 60, 
        position: 'absolute', 
        right: 0,
        paddingVertical: 7,
        paddingRight: 10, 
        
    },
    textSpacing: {
        flex: 1,
        paddingVertical: 1,
        paddingHorizontal: 15,
    },
    text : {
        fontSize: 20,
        color: '#2e78b7',
    }
})
import React from 'react';
import { Dropdown } from 'react-native-material-dropdown';
import { TextField } from 'react-native-material-textfield';
import  DatePicker  from 'react-native-datepicker';
import { TextButton, RaisedTextButton } from 'react-native-material-buttons';
import { MapView, ImagePicker, WebBrowser } from 'expo';
import axios from 'axios';
import Paths from '../constants/Paths';

import PropTypes from 'prop-types';

import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  Switch,
  TouchableOpacity,
  View,
  Button, // for upload image
} from 'react-native';

export default class LocationSearch extends React.Component {
    constructor(props) {
        super(props)
    }

    state = {
      region: {
        latitude: 38.9897,
        longitude: -76.9378,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    }

    static propTypes = {
        onBack : PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
    }

    componentDidMount () {
        console.log("mounted")
    }

    _onBack () {
      this.props.onBack()
    }

    _onSubmit () {
      this.props.onSubmit(this.state.region.latitude, this.state.region.longitude)
    }

    render ( ) {
      let {region} = this.state
        return (
              <View style={{flex: 1, flexDirection: 'col',}}>
                    {/*Map goes here*/}

                      <MapView
                        style={styles.map}
                        initialRegion={{
                          latitude: 38.9897,
                          longitude: -76.9378,
                          latitudeDelta: 0.0922,
                          longitudeDelta: 0.0421,
                        }}
                        onRegionChange={(region) => this.setState({region})}
                      >
                      <MapView.Marker
                        coordinate={
                          {latitude: this.state.region.latitude,
                          longitude: this.state.region.longitude,}
                        }
                        title={"HERE!!"}
                        description={"I am HERE!!"}
                      />

                      </MapView>



                <View style={{flex: 1, flexDirection: 'row', padding: 20}}>
                  {/*Controlls go here */}
                  <View style={{flex: 1, padding: 20 }}>
                    <RaisedTextButton
                        title='Submit'
                        onPress={this._onSubmit.bind(this)}
                    />
                  </View>
                  <View style={{flex: 1, padding: 20}}>
                    <RaisedTextButton
                        title='Back'
                        onPress={this._onBack.bind(this)}
                    />
                  </View>

                </View>


            </View>
        )
    }
}


const styles = StyleSheet.create({
    getMap: {
      paddingVertical: 10,
      flex: 1,
      textAlign: 'center',
      marginHorizontal: 10,
    },

    map: {
      height: 300,  /* The height is 400 pixels */
      width: '100%',  /* The width is the width of the web page */

    },

    container: {
      flex: 1,
      flexDirection: 'row',
      paddingTop: 15,
      backgroundColor: '#fff',
    },
    padding: {
      paddingHorizontal: 15,
      paddingVertical: 15
    },
    buttons: {

    },

    //Doesn't WORK!!!!
    floatingContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      maxHeight: 220,
      ...Platform.select({
        ios: {
          shadowColor: 'black',
          shadowOffset: { height: -3 },
          shadowOpacity: 0.1,
          shadowRadius: 3,
        },
        android: {
          elevation: 20,
        },
      }),

      backgroundColor: '#fbfbfb',
      paddingVertical: 20,
    },

  });

import React from 'react'
import {View, Text, FlatList, Switch, ScrollView, StyleSheet, Platform} from 'react-native'
import axios from 'axios'
import PropTypes from 'prop-types'
import Paths from '../constants/Paths'
import SwitchSelection from './SwitchSelection'
import { Dropdown } from 'react-native-material-dropdown'
import { RaisedTextButton } from 'react-native-material-buttons';

class Category {
    constructor(name, parent, id, isSubbed) {
        this.name = name
        this.parent = parent
        this.id = id
        this.isSubbed = isSubbed
    }
}

export default class CategorySelector extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.updateElementsFromServer(this.props.parent)
    }

    static propTypes = {
        parent : PropTypes.string.isRequired,
        subbedTo: PropTypes.array.isRequired,
        onComplete: PropTypes.func.isRequired, // When youre done selecting categories
        onSubscribe: PropTypes.func.isRequired // When you selected one catagory
    }


    state = {
        elements: []
    }

    _onReset = () => {
        this.updateElementsFromServer(this.props.parent)
    }

    _onSelect = (newParent) => {
        this.updateElementsFromServer(newParent)
    }

    updateElementsFromServer = (parent) => {
        /*let elements = []
        if (parent == "Italian") {
            elements.push(new Category("Spag", parent, "id1" ))
            elements.push(new Category("Lasagna", parent, "id2" ))
            elements.push(new Category("Marinera???", parent, "id1" ))
        } 
        else if (parent == "Spag") {
            elements.push(new Category("Spicey", parent, "id1" ))
            elements.push(new Category("uhh, bland?", parent, "id1" ))
        }
        else {
            elements.push(new Category("Italian", parent, "id1" ))
            elements.push(new Category("American", parent, "id2"))
            elements.push(new Category("French", parent, "id3"))
        }
        
        this.setState({elements: elements})
        return*/

        axios.get(Paths.serverUrl + "/children?parent=" + parent)
            .then(res => {
                let elementList = []
                console.log("Getting categories of parent: " + parent)
                console.log(res.data)
                res.data.forEach(e => {
                    elementList.push(new Category(e, parent))
                })
                // TODO: add the category conversion
                this.setState({elements: elementList})
            }
        )
    }

    sendNewCatagoryToServer = () => {}

    _renderItem = e => {
        // TODO: add a click to render next category
        let name = e.name.toString()
        let value = this.props.subbedTo.indexOf(name) >= 0 
        console.log(name + " => " + value)
        return (
            <SwitchSelection key={name+"Key"}
                title= {name}
                value= {value}
                onToggle= {this.props.onSubscribe.bind(this)}
                onSelect= {this._onSelect.bind(this)}
            />
        )
    }

    render () {
        return (
            <View style={{flex: 1 }}>
                <ScrollView style={{flex: 1}}>
                {
                    this.state.elements.map(this._renderItem.bind(this))

                }
                    
                </ScrollView>

                <View style={styles.floatingContainer}>
                    <View style={{paddingLeft: 25}}>
                        <RaisedTextButton
                            title = "Save Selection"
                            onPress = {this.props.onComplete}
                        />
                    </View>
                    <View style={{paddingRight: 25}}>
                        <RaisedTextButton
                            title = "Reset View"
                            onPress = {this._onReset.bind(this)}
                        />
                    </View>
                </View>
            </View>
        )
        // Select button 
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      paddingTop: 15,
      backgroundColor: '#fff',
    },
    padding: {
      paddingHorizontal: 15,
      paddingVertical: 15 
    },
    floatingContainer: {
        flex: 1, 
        flexDirection: 'row', 
        justifyContent: 'space-between',
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      maxHeight: 220,
      ...Platform.select({
        ios: {
          shadowColor: 'black',
          shadowOffset: { height: -3 },
          shadowOpacity: 0.1,
          shadowRadius: 3,
        },
        android: {
          elevation: 20,
        },
      }),
      
      backgroundColor: '#fbfbfb',
      paddingVertical: 20,
    },
  
  });
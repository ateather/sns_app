import React from 'react';
import { TextField } from 'react-native-material-textfield';
import { TextButton, RaisedTextButton } from 'react-native-material-buttons';
import CheckBox from 'react-native-check-box'
import axios from 'axios'
import Paths from '../constants/Paths'
import { Constants, Location, Permissions } from 'expo';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  StatusBar,
  Switch,
  Text,
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
} from 'react-native';
import { WebBrowser } from 'expo';

import { MonoText } from '../components/StyledText';
import {Publisher, User} from '../navigation/AppNavigator';

export default class LoginScreen extends React.Component {
    static MessageOptions = {
        content : "",
    };

    static signUpOptions = {
        content : "",
        check : false,
    }

    state = {
        isVerify: null,
        isLoggedIn: false,
        currentUser: '',
        password: '',
        isPublisher: false,
        signUp: false,
        newFirst: '',
        newLast: '',
        newEmail: '',
        newPassword: '',
        checkPassword: '',
        newPublisher: false,
        location: null,
        lat: 0,
        long: 0
    };

    componentWillMount() {
        if (Platform.OS === 'android' && !Constants.isDevice) {
          this.setState({
            errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
          });
        } else {
          this._getLocationAsync();
        }
      }

      _getLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
          console.log("Error getting location permission")
        }

        let location = await Location.getCurrentPositionAsync({});
        //console.log(location)
        this.setState({
            lat: location.coords.latitude,
            long: location.coords.longitude
        });
      };

    _onSignOut = () => {
        this.setState({
            isVerify: null,
            isLoggedIn: false,
            currentUser: '',
            password: '',
            isPublisher: false,
            signUp: false,
            newFirst: '',
            newLast: '',
            newEmail: '',
            newPassword: '',
            checkPassword: '',
            newPublisher: false,
            location: null,
            lat: 0,
            long: 0
        })
        this.MessageOptions = {
            content : "Signed Out"
        }
    }

    _handlePressButtonAsync = async () => {
        let result = await WebBrowser.openBrowserAsync(Paths.serverUrl + this.state.isVerify);
        this.setState({
            isVerify: null,
            signUp: false
        })
        console.log(result)
    };

    render() {
        let mainPart;
        let st = JSON.stringify(this.state, null, 4);
        if (this.state.isVerify) {
            mainPart =
            <ScrollView style={styles.container}>
                <TouchableOpacity style={{paddingVertical: 50, paddingHorizontal: 20}} onPress={this._handlePressButtonAsync.bind(this)}>
                    <Text>{this.state.isVerify}</Text>
                </TouchableOpacity>
            </ScrollView>
        }
        else if (this.state.signUp) {
          mainPart =
          <KeyboardAvoidingView
           style={styles.container}
           behavior="padding"
         >
          <View style={styles.container}>
            <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>

            <View style={styles.welcomeContainer}>
                {this._signUpMessage()}
            </View>

            {/*All the contents in State*/}
            {/*<Text> Users: {st} </Text>*/}

            <TextField
                containerStyle={styles.contentPadding}
                onChangeText={ (first) => this.setState({ newFirst: first }) }
                value={this.state.newFirst}
                label='First Name'
                title='First Name'
            />

            <TextField
                containerStyle={styles.contentPadding}
                onChangeText={ (last) => this.setState({ newLast: last }) }
                value={this.state.newLast}
                label='Last Name'
                title='Last Name'
            />

              <TextField
                  containerStyle={styles.contentPadding}
                  onChangeText={ (email) => this.setState({ newEmail: email }) }
                  value={this.state.newEmail}
                  label='NewEmail'
                  title='ex@mple.com'
              />

              <CheckBox
                  style={{flex: 1, paddingHorizontal: 30}}
                  onClick={()=>{
                    this.setState({
                        isPublisher: !this.state.isPublisher
                    })
                  }}
                  isChecked={this.state.isPublisher}
                  rightText={"Are you a Publisher?"}
              />

              <TextField
                  containerStyle={styles.contentPadding}
                  onChangeText={ (pass) => this.setState({ newPassword: pass }) }
                  value={this.state.newPassword}
                  label='Password'
              />

              <TextField
                  containerStyle={styles.contentPadding}
                  onChangeText={ (pass) => this.setState({ checkPassword: pass }) }
                  value={this.state.checkPassword}
                  label='Check Password'
              />

              <View style={styles.padded}>
                  <RaisedTextButton style={styles.contentPadding}
                      title='Sign Up'
                      onPress={this._onCreate}
                  />
              </View>

              <View style={{flex: 1, flexDirection: 'row',...styles.padded}}>
                <Text> Switch to Login: </Text>
                <Switch
                  onValueChange={ this._onSwitch }
                  value={ this.state.signUp }
                />
              </View>

            </ScrollView>
          </View>
        </KeyboardAvoidingView>
        } else {
          mainPart =
          <View style={styles.container}>
              <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>

                  <View style={styles.welcomeContainer}>
                      {this._renderMessage()}
                  </View>

                  {/*All the contents in State*/}
                  {/*<Text> Users: {st} </Text>*/}

                  <TextField
                      containerStyle={styles.contentPadding}
                      onChangeText={ (email) => this.setState({ currentUser: email }) }
                      value={this.state.currentUser}
                      label='Email'
                      title='ex@mple.com'
                  />

                  <TextField
                      containerStyle={styles.contentPadding}
                      onChangeText={ (pass) => this.setState({ password: pass }) }
                      value={this.state.password}
                      label='Password'
                  />

                  <View style={styles.padded}>
                      <RaisedTextButton style={styles.contentPadding}
                          title='Submit'
                          onPress={this._onSubmit}
                      />
                  </View>

                  <View style={{flex: 1, flexDirection: 'row',...styles.padded}}>
                    <Text> Switch to Sign Up: </Text>
                    <Switch
                      onValueChange={ this._onSwitch }
                      value={ this.state.signUp }
                    />
                  </View>

              </ScrollView>
          </View>
        }

        if (this.state.isLoggedIn) {
            if (this.state.isPublisher) {
                return (
                    <View style={styles.container}>
                        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                        <Publisher
                            screenProps = {{
                                user : this.state.currentUser,
                                isPublisher : true,
                                onSignOut : this._onSignOut.bind(this)
                            }}
                        />
                    </View>
                );
            } else {
                return (
                    <View style={styles.container}>
                        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                        <User
                            screenProps = {{
                                user : this.state.currentUser,
                                isPublisher : false,
                                onSignOut : this._onSignOut.bind(this)
                            }}
                        />
                    </View>

                );
            }
        } else {
          return mainPart;
        }
    }

    _renderMessage() {
        if (this.MessageOptions == null
            || this.MessageOptions.content == '') {
              if (this.signUpOptions != null
                  && this.signUpOptions.check) {
                  return (
                    <View style={styles.getStartedContainer}>
                        <Text style={styles.getStartedText}>
                            Login!
                        </Text>

                        <MonoText style={styles.codeHighlightText}>
                            User created succesfully!
                        </MonoText>
                    </View>
                );
              } else {
                return (
                  <View style={styles.getStartedContainer}>
                      <Text style={styles.getStartedText}>
                          Login!
                      </Text>
                  </View>
                );
              }
        } else {
            return (
                <View style={styles.getStartedContainer}>
                    <Text style={styles.getStartedText}>
                            Login Failed
                    </Text>
                    <MonoText style={styles.codeHighlightText}>
                        {this.MessageOptions.content}
                    </MonoText>
                </View>
            );
        }
    }

    _onSubmit = () => {
        if (this.state.currentUser == null
            || this.state.password == null
            || this.state.currentUser == ''
            || this.state.password == '') {
            this.MessageOptions = {
                content : "Empty Fields"
            }
            this.setState({isLoggedIn: false});
        } else {

            let message = {
                email : this.state.currentUser,
                pass : this.state.password,
                lat : this.state.lat,
                long : this.state.long
            }
            axios.post(Paths.serverUrl + '/login', message)
                .then((res, err) => {
                    console.log(res.data)
                    if (res.data.succ) {
                        this.setState({
                            isLoggedIn: true,
                            isPublisher: res.data.isPublisher
                        });
                    } else {
                        this.MessageOptions = {
                            content : res.data.m
                        }
                    }
                })
                .catch(err => {
                    this.MessageOptions = {
                        content : "Invalid Login"
                    }
                })


        }
    }

    _signUpMessage = () => {
        if (this.signUpOptions == null) {
              return (
                <View style={styles.getStartedContainer}>
                    <Text style={styles.getStartedText}>
                        Creating New User!
                    </Text>
                </View>
            );
        } else {
            return (
              <View style={styles.getStartedContainer}>
                  <Text style={styles.getStartedText}>
                      Creating New User Failed!
                  </Text>

                  <MonoText style={styles.codeHighlightText}>
                      {this.signUpOptions.content}
                  </MonoText>
              </View>
            );
        }
    }

    // Check all the contents of Sign Up and Submit
    _onCreate = () => {
        if (this.state.newLast == null
            || this.state.newFirst == null
            || this.state.newEmail == null
            || this.state.newPassword == null
            || this.state.newLast == ''
              || this.state.newFirst == ''
              || this.state.newEmail == ''
              || this.state.newPassword == '') {
              this.signUpOptions = {
                content : "Empty Fields",
                check : false,
              }
              this.setState({signUp: true});
        } else if (this.state.newPassword !== this.state.checkPassword) {
            this.signUpOptions = {
              content : "Passwords didn't Match",
              check : false,
            }
            this.setState({signUp: true});
        } else {
            this.signUpOptions = {
              content : "",
              check : true,
            }

            // POST to server
            axios.post(Paths.serverUrl + '/register', this.state)
                .then((res, err) =>{
                    console.log(res.data)

                    if (res.data.link) {
                        this.setState({isVerify: res.data.link})
                    } else {
                        this.signUpOptions = {
                            content : "Email in use",
                            check : false,
                        }
                        this.setState({signUp: true});
                    }

                    // this.setState({signUp: false});
                })






        }
    }

    _onSwitch = (value) => {
      this.setState({
        isLoggedIn: false,
        currentUser: '',
        password: '',
        isPublisher: false,
        signUp: value,
        newFirst: '',
        newLast: '',
        newEmail: '',
        newPassword: '',
        checkPassword: '',
        newPublisher: false,
      });
    }


}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    contentPadding: {
      paddingHorizontal: 30,
    },
    developmentModeText: {
      marginBottom: 20,
      color: 'rgba(0,0,0,0.4)',
      fontSize: 14,
      lineHeight: 19,
      textAlign: 'center',
    },
    contentContainer: {
      paddingTop: 30,
    },
    padded: {
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
    welcomeContainer: {
      alignItems: 'center',
      marginTop: 10,
      marginBottom: 20,
    },
    welcomeImage: {
      width: 100,
      height: 80,
      resizeMode: 'contain',
      marginTop: 3,
      marginLeft: -10,
    },
    getStartedContainer: {
      alignItems: 'center',
      marginHorizontal: 50,
    },
    homeScreenFilename: {
      marginVertical: 7,
    },
    codeHighlightText: {
      color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
      backgroundColor: 'rgba(0,0,0,0.05)',
      borderRadius: 3,
      paddingHorizontal: 4,
    },
    getStartedText: {
      fontSize: 17,
      color: 'rgba(96,100,109, 1)',
      lineHeight: 24,
      textAlign: 'center',
    },
    tabBarInfoContainer: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      ...Platform.select({
        ios: {
          shadowColor: 'black',
          shadowOffset: { height: -3 },
          shadowOpacity: 0.1,
          shadowRadius: 3,
        },
        android: {
          elevation: 20,
        },
      }),
      alignItems: 'center',
      backgroundColor: '#fbfbfb',
      paddingVertical: 20,
    },
    tabBarInfoText: {
      fontSize: 17,
      color: 'rgba(96,100,109, 1)',
      textAlign: 'center',
    },
    navigationFilename: {
      marginTop: 5,
    },
    helpContainer: {
      marginTop: 15,
      alignItems: 'center',
    },
    helpLink: {
      paddingVertical: 15,
    },
    helpLinkText: {
      fontSize: 14,
      color: '#2e78b7',
    },
  });

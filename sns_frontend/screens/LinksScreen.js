import React from 'react';
import {   Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View} from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import { NavigationEvents } from 'react-navigation';
import { TextButton, RaisedTextButton} from 'react-native-material-buttons';
import StoryPreview from '../components/StoryPreview';
import CategorySelector from '../components/CategorySelector';
import LocationSearch from '../components/LocationSearch';
import ContentsSearch from '../components/ContentsSearch'; // ContentsSearch Part
import axios from 'axios';
import Paths from '../constants/Paths';
import { MonoText } from '../components/StyledText';


class StoryPreviewData {
  constructor (id, title, icon) {
      this.title = title
      this.icon = icon
      this.id = id
  }
}


export default class LinksScreen extends React.Component {
  static INACTIVE_STORY = -1
  constructor(props) {
    super(props)

  }

  componentDidMount() {
    /*this._getStoriesFromServer()
    this._gatherSubbedToList(this.props.screenProps.user)*/
  }


  static navigationOptions = {
    title: 'Stories',
  };

  state = {
    viewingCatagory: null,
    stories: [],
    fullStories: [],
    viewStoryId: -1,
    activeStory: null,
    changeLocation: false,
    searchContents: false,
    subbedTo: []

  }
  
  _onFocus () {
    console.log("Focused")
    this._getStoriesFromServer()
    this._gatherSubbedToList(this.props.screenProps.user)
  }

  _gatherSubbedToList (user) {
    //console.log(user)
    let result = []
    axios.get(Paths.serverUrl + '/preferences?email=' + this.props.screenProps.user)
      .then(res => {
        console.log("Preferences: " + res.data)
        //this.state.subbedTo = res.data
        this.setState({subbedTo: res.data})
      })
  }

  _navigateToChangeLocation () {
    console.log("Naving to change location")
    this.setState({changeLocation: true})
  }

  //Searching Contents Part
  _navigateToSearchContents () {
    console.log("Searching contents")
    this.setState({searchContents: true})
  }

  _navigateToChangeSubscriptions () {
    console.log("Loading root")
    //navigationOptions.title = "Subscriptions"
    this.setState({viewingCatagory: "root"})
  }

  _doneSubscribing () {
    console.log("Categories Subscribed")
    //navigationOptions.title = "Stories"
    this._getStoriesFromServer()
    this.setState({viewingCatagory: null})
  }

  _subscriptionChange (cid, value) {
    let subbedTo = this.state.subbedTo
    console.log("[" + cid + "] => " + value)
    if (value)
      subbedTo.push(cid)
    else {
      let i = subbedTo.indexOf(cid)
      if (i >= 0)
        subbedTo.splice(i, 1)
    }

    // DEBUG: Post change to server for user
    let message = {
      email: this.props.screenProps.user,
      cat_name: cid,
      value: value
    }
    axios.post(Paths.serverUrl + "/preferences", message)
      .then(res => {
        console.log("[" + cid + "] => " + value + " stored!")
      })
  }

  _getActiveStory(id) {
    this.setState({
      viewStoryId: id,
      activeStory: this.state.fullStories[id]
    })
  }

  _clearStory () {
    this.setState({viewStoryId: -1, activeStory: null})
  }
  _viewStory (id) {
    this._getActiveStory(id)
  }

  _getStoriesFromServer () {
    //console.log("--eee---")
    this.setState(
    {
      stories: [],
      fullStories: []
    })
    axios.get(Paths.serverUrl+'/userstories?email=' + this.props.screenProps.user)
      .then(res => {
        console.log("-----")
        console.log(res.data)
        let fS = []
        let s = []
        res.data.forEach(story => {
          console.log(JSON.stringify(story.category_id))
          let m = JSON.parse(story.media)
          let fs = {
            title: story.title,
            images: m,
            message: story.message_content,
            id: story.title,
            date: story.expiration_date,
            publisher: story.pub_email,
            categories: story.category_id
          }
          fS[story.title] = fs
          s.push(new StoryPreviewData(story.title, story.title, m[0]))
        })
        this.setState(
        {
          stories: s,
          fullStories: fS
        })
        //this.setState({stories: res.data})
      })
      .catch(err => {
        console.log("err: ")
        console.log(err)
      })
  }

  _doneWithLocationSearch () {
    this.setState({changeLocation: false})
  }

  //Search Contents Done
  _doneWithSearchContents () {
    this.setState({searchContents: false})
  }

  _submitNewLocation (lat, long) {
    let message = {
      email: this.props.screenProps.user,
      lat: lat,
      long: long,
    }


    axios.post(Paths.serverUrl + "/location", message)
      .then(done => {
        this._doneWithLocationSearch()
        this._getStoriesFromServer()
      })
  }

  //Submit the title and messages
  _submitContents (ctitle, cmessage, categories, lat, long) {
    let contents = {
      email: this.props.screenProps.user,
      title: ctitle,
      message: cmessage,
      categories: categories,
      lat : lat,
      long : long,
    }
    axios.post(Paths.serverUrl + "/search", contents)
      .then(res => {
        console.log(res.data)
        console.log("-----")
        console.log(res.data)
        let fS = []
        let s = []
        res.data.forEach(story => {
          console.log(JSON.stringify(story.category_id))
          let m = JSON.parse(story.media)
          let fs = {
            title: story.title,
            images: m,
            message: story.message_content,
            id: story.title,
            date: story.expiration_date,
            publisher: story.pub_email,
            categories: story.category_id
          }
          fS[story.title] = fs
          s.push(new StoryPreviewData(story.title, story.title, m[0]))
        })
        this.setState(
        {
          stories: s,
          fullStories: fS
        })
        //this.setState({stories: res.data})
        this._doneWithSearchContents()
        
      })

  }

  render() {
    if (this.state.changeLocation) {

      return (

        <View>
          <LocationSearch
            onBack = {this._doneWithLocationSearch.bind(this)}
            onSubmit = {this._submitNewLocation.bind(this)}
          />
        </View>
      )
    }
    else if (this.state.viewingCatagory != null) {
      return (
        <CategorySelector
          parent = {this.state.viewingCatagory}
          onComplete = {this._doneSubscribing.bind(this)}
          onSubscribe = {this._subscriptionChange.bind(this)}
          subbedTo = {this.state.subbedTo}
        />
      )
    }
    // Search Contents Part
    else if (this.state.searchContents) {
      return (

          <ContentsSearch
            onBack = {this._doneWithSearchContents.bind(this)}
            onSubmit = {this._submitContents.bind(this)}
          />

      )
    }
    else if (this.state.viewStoryId != -1) {
      return (
        <View style={{flex: 1, ...styles.padding}}>
          <ScrollView style={{paddingHorizontal: 15, maxHeight: 440, ...styles.container}}>

              <View>
                <Text>{this.state.activeStory.title}</Text>

                <View style= {{width: 200}}>
                  {

                    this.state.activeStory.images.map((img, i) => (
                      <Image key={i}
                        style = {{width: 200, height: 200}}
                        source = {{uri: img}}
                      />
                    ))
                  }
                </View>
              </View>

              <View>
                <Text>{this.state.activeStory.message}</Text>
              </View>

              <View>
                <Text>
                  Publisher: 
                </Text>
                <MonoText style={styles.codeHighlightText}>
                  {this.state.activeStory.publisher}
                </MonoText>
              </View>

              <View>
                <Text>
                  Categories: 
                </Text>
                <MonoText style={styles.codeHighlightText}>
                  {this.state.activeStory.categories}
                </MonoText>
              </View>

              <View>
                <Text>
                  Expiration: 
                </Text>
                <MonoText style={styles.codeHighlightText}>
                  {this.state.activeStory.date}
                </MonoText>
              </View>
          </ScrollView>
          <View style={styles.floatingContainer}>
            <RaisedTextButton
              title = "<- Choose Another Story"
              onPress= {this._clearStory.bind(this)}
            />
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <NavigationEvents
            onWillFocus={this._onFocus.bind(this)}
          />
          <ScrollView style={styles.container}>

            <View>
              <View style={styles.padding}>
                <RaisedTextButton
                  title= "Change Subscriptions"
                  onPress= {this._navigateToChangeSubscriptions.bind(this)}
                />
              </View>

              <View style={styles.padding}>
                <RaisedTextButton
                  title= "Change Location"
                  onPress= {this._navigateToChangeLocation.bind(this)}
                />
              </View>

              <View style={styles.padding}>
                <RaisedTextButton
                  title= "Search Stories"
                  onPress= {this._navigateToSearchContents.bind(this)}
                />
              </View>
          </View>

          </ScrollView>

          <View style={styles.floatingContainer}>
            <ScrollView style= {styles.container}>

              {
                this.state.stories.map(e => (
                  <StoryPreview key = {e.id}
                    id = {e.id}
                    title = {e.title}
                    icon = {e.icon}
                    onSelect= {this._viewStory.bind(this)}
                  />
                ))
              }

            </ScrollView>
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  padding: {
    paddingHorizontal: 15,
    paddingVertical: 15
  },
  floatingContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    maxHeight: 270,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  codeHighlightText: {
    paddingLeft: 10,
    color: 'rgba(96,100,109, 0.8)',
  }

});

import React from 'react';
import { ExpoConfigView } from '@expo/samples';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import axios from 'axios'
import Paths from '../constants/Paths'

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'User Settings',
  };

  _onDeleteUser = () => {
    // POST
    axios.post(Paths.serverUrl + '/delete', {email : this.props.screenProps.user})
      .then(res => {
        alert(res.data.m)
        this.props.screenProps.onSignOut()
      })
  }

  render() {
    /* Go ahead and delete ExpoConfigView and replace it with your
     * content, we just wanted to give you a quick view of your config */
    //return <ExpoConfigView />;

    return (
      <ScrollView style={styles.container}>
        <TouchableOpacity style={{alignContent: 'center', paddingVertical: 50}} onPress={this.props.screenProps.onSignOut}>
            <Text style={styles.helpLinkText}>Sign Out</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{alignContent: 'center', paddingVertical: 50}} onPress={this._onDeleteUser.bind(this)}>
            <Text style={styles.helpLinkText}>Delete User</Text>
        </TouchableOpacity>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: '#fff',
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});



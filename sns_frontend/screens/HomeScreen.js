import React from 'react';
import { Dropdown } from 'react-native-material-dropdown';
import { TextField } from 'react-native-material-textfield';
import  DatePicker  from 'react-native-datepicker';
import AwesomeButton from 'react-native-really-awesome-button';
import { MapView, ImagePicker, WebBrowser } from 'expo';
import axios from 'axios';
import Paths from '../constants/Paths';

import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  Switch,
  TouchableOpacity,
  View,
  Button, // for upload image
} from 'react-native';

import { MonoText } from '../components/StyledText';

export default class HomeScreen extends React.Component {
  //DateTimePicker
  state = {
    allCategories: [],
    enterMessage: '',
    enterTitle: '',
    DropdownCategory: '',
    enterNewCategory: '',
    enterAddCategories: '', //Variable that stores the additional categories 
    SubOrNot: false,
    isDateTimePickerVisible: false,
    date: new Date(),
    enterRange: '',
    region: {
      latitude: 38.9897,
      longitude: -76.9378,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    },
    image: [],
  };

  componentDidMount() {
    
    axios.get(Paths.serverUrl + "/category").then(res => {
      let data = res.data;
      this.setState({allCategories: data});
    })
    

    /*let DUB_BAR = []
    DUB_BAR.push({
      cat_name: 'B',
      cat_parent: null,
    })
    this.setState({allCategories: DUB_BAR})
    */
  }


  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    console.log('A date has been picked: ', date);
    this._hideDateTimePicker();
  };


  static navigationOptions = {
    header: null,
  };

  //The function used to set the initial Region
  getInitialState() {
    return {
      region: {
        latitude: 38.9897,
        longitude: -76.9378,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    };
  }

  _pickImage = async () => {
    const { CAMERA_ROLL, Permissions } = Expo;
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status === 'granted') {
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3],
      });



      if (!result.cancelled) {
        let imageArray = this.state.image;
        imageArray.push(result.uri);
        this.setState({ image: imageArray});
      }
    } else {
      throw new Error('CAMERA_ROLL permission not granted');
    }
  };


  render() {
    //Categories
    let parentCategory1 = [{
      value: 'Banana',
    }, {
      value: 'Mango',
    }, {
      value: 'Pear',
    }];

    let parentCategory = this.state.allCategories.map(e=> {
      return {value: e.cat_name}
    });

    let {enterMessage} = this.state;
    let {enterTitle} = this.state;
    let {enterNewCategory} = this.state;
    let {SubOrNot} = this.state;
    let {enterRange} = this.state;
    let {region} = this.state;
    let {image} = this.state;
    let {DropdownCategory} = this.state;
    let {enterAddCategories} = this.state;

    let subCat;
    if(SubOrNot) {
      subCat = <TextField
        label='New SUB Category' value={enterNewCategory}
        onChangeText={ (enterNewCategory) => this.setState({ enterNewCategory }) }
        containerStyle={styles.messageStyle}
      />;
    } else {
      subCat = <TextField
        label='New Category' value={enterNewCategory}
        onChangeText={ (enterNewCategory) => this.setState({ enterNewCategory }) }
        containerStyle={styles.messageStyle}
      />;
    }



    // Return part
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.welcomeContainer}>
            <Image
              source={
                __DEV__
                  ? require('../assets/images/robot-dev.png')
                  : require('../assets/images/robot-prod.png')
              }
              style={styles.welcomeImage}
            />
          </View>

          <View style={styles.getStartedContainer}>
            <Text style={styles.getStartedText}>Create a New Story!</Text>
          </View>

          {/* Message part */}
          <View stretch style={styles.getMessagePart}>
            {/* Title of the Message */}
            <TextField
              label='Title' value={enterTitle}
              onChangeText={ (enterTitle) => this.setState({ enterTitle }) }
              containerStyle={styles.messageTitleStyle}
            />

            { /* Contents of the Message */}
            <TextField
              label='Message' value={enterMessage}
              onChangeText={ (enterMessage) => this.setState({ enterMessage }) }
              containerStyle={styles.mainMessageStyle}
              multiline={true}
            />
          </View>

          {/* Upload images */}
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button
              title="Pick images from camera roll"
              onPress={this._pickImage}
            />
              {image &&
              <Image source={{ uri: this.state.image[this.state.image.length - 1] }} style={{ width: 80, height: 80 }} />}
              <Text style={styles.getStartedText}>Number of photoes: {this.state.image.length}</Text>
          </View>

          <View style={styles.getCategoryPart}>
            { /* Category of the Message & new Category input */}
            <Dropdown
              label='Category'
              data={parentCategory}
              onChangeText={ (data) => this.setState( {DropdownCategory: data} )}
              containerStyle = {styles.dropStyle}
            />
            {subCat}
          </View>
          <View style={styles.getSubCategory}>
            <Text style={styles.SubCatText}>Is the new Category a SubCategory?:  </Text>
            <Switch
              onValueChange={ (value) => this.setState({ SubOrNot: value })}
              value={ this.state.SubOrNot }
            />



            { /* New SubCategory input
            <Text style={styles.getStartedText}>Want to create a New Sub Category?:</Text>
            <TextField
              label='New Sub Category' value={enterNewSubCategory}
              onChangeText={ (enterNewSubCategory) => this.setState({ enterNewSubCategory }) }
              containerStyle={styles.messageStyle}
            />

          */}
          </View>

          <View style={styles.getCategoryPart}>
            <TextField
              label='Additional Categories!' value={enterAddCategories}
              onChangeText={ (enterAddCategories) => this.setState({ enterAddCategories }) }
              title='Note: Please seperate them by commas (",")'
              containerStyle={styles.messageStyle}
            />
          </View>

          <View style={styles.getDateTimePickerPart}>

            <Text style={styles.getStartedText}>Avaliable until: </Text>
            <DatePicker
              style={{width: 200}}
              date={this.state.date}
              mode="datetime"
              placeholder="select date"
              format="lll"
              minDate= {new Date()}
              maxDate="2118-12-31"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 36
                }
              }}
              onDateChange={(date) => {this.setState({date: date})}}
            />
            </View>

            <View style={styles.getRange}>
            <Text style={styles.getStartedText}>Ranging your message:</Text>
            <TextField
              label='Range' type='numeric' value={enterRange}
              onChangeText={ (enterRange) => this.setState({ enterRange }) }
              containerStyle={styles.messageStyle}
            />


          </View>

          <View style={styles.getMap}>
          <Text>Scroll the Map to Find Your Position</Text>
          <MapView
            style={styles.map}
            initialRegion={{
              latitude: 38.9897,
              longitude: -76.9378,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            onRegionChange={(region) => this.setState({region})}
          >
          <MapView.Marker
            coordinate={
              {latitude: this.state.region.latitude,
              longitude: this.state.region.longitude,}
            }
            title={"HERE!!"}
            description={"I am HERE!!"}
          />

          </MapView>
          </View>

          <View style={{flex:1, alignItems: 'center',margin: 10,}}>
          <AwesomeButton onPress={this._handleHelpPress} backgroundColor='#2e78b7' >Submit</AwesomeButton>
          </View>

          {/* Looking elements in state*/}
          {/*<Text>{this.state.region.latitude.toString() + " "
                + this.state.image[this.state.image.length - 1] + " "
                + this.state.date.toString() + " "
                + enterTitle + " " + enterMessage + " " + DropdownCategory + " " + enterNewCategory + " " + enterRange + " "
                + SubOrNot.toString()}
          </Text> */}

        </ScrollView>


      </View>
    );
  }

  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    //console.log(this.state)
    // Todo: valiated datas
    let story = {
      message: this.state.enterMessage,
      title: this.state.enterTitle,
      DropdownCategory: this.state.DropdownCategory,
      NewCategory: this.state.enterNewCategory,
      subOrNot: this.state.SubOrNot,
      extraCategories: this.state.enterAddCategories,
      date: this.state.date,
      range: this.state.enterRange,
      lat: this.state.region.latitude,
      long: this.state.region.longitude,
      image: this.state.image,
      publisher: this.props.screenProps.user,
    };

    //console.log(this.message)

    axios.post(Paths.serverUrl + "/story", story).then(res => {
      console.log(res.data)
      if (res.data.succ) {
        alert("Story Published!")
        /*this.setState({
          allCategories: [],
          enterMessage: '',
          enterTitle: '',
          DropdownCategory: '',
          enterNewCategory: '',
          enterAddCategories: '', //Variable that stores the additional categories 
          SubOrNot: false,
          isDateTimePickerVisible: false,
          date: new Date(),
          enterRange: '',
          region: {
            latitude: 38.9897,
            longitude: -76.9378,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          },
          image: [],
        })*/
      }
    }).catch(err => {
      console.log(err)
    })



  };

  _navigateToNextPage = () => {
    this.navigation.navigate("Screen2")
  };
}

const styles = StyleSheet.create({

  dropStyle: {
    flex: 1,
    marginHorizontal: 10,
  },

  messageStyle: {
    flex: 2,
    marginLeft: 10,
    marginRight: 10,
  },
  messageTitleStyle: {
    flex: 1,
    marginHorizontal: 0,
  },
  mainMessageStyle: {
    flex: 2,
    marginLeft: 10,
  },

  getMessagePart: {
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: 10,
  },

  getCategoryPart: {
    alignItems: 'center',
    flexDirection: 'row',
    flex: 2,
    marginBottom: 10,
  },

  getSubCategory: {
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
    marginBottom: 10,
  },

  getRange: {
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
    marginHorizontal: 10,
  },

  getRangeInput: {
  },

  getDateTimePickerPart: {
    alignItems: 'center',
    marginHorizontal: 10,
    flex: 2,
    flexDirection: 'row',
  },

  SubCatText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
    marginHorizontal: 10,
  },

  getMap: {
    flex: 1,
    textAlign: 'center',
    marginHorizontal: 10,
  },

  map: {
    height: 300,  /* The height is 400 pixels */
    width: '100%',  /* The width is the width of the web page */

  },


  container: {
    flex: 1,
    backgroundColor: '#fff',
  },

  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});

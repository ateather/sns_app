window.onload = main;

var Points = [];
var latitude = [];
var longitude = [];
var address;
var count = 0;
var marker;
var geocoder;

var centerLL;
var myMap;
var map;

function setLL() {
  var lat = document.getElementById("lat").value;
  var lng = document.getElementById("lng").value;
  if (!isNaN(lat) && !isNaN(lng)
      && parseFloat(lat) >= -85 && parseFloat(lat) <= 85
      && parseFloat(lng) >= -180 && parseFloat(lng) <= 180) {
    latitude[count] = parseFloat(lat);
    longitude[count] = parseFloat(lng);
    let newPoint = {
      latitude: latitude[count],
      longitude: longitude[count]
    };
    Points.push(newPoint);
    count++;
    setMarker();
  } else {
    alert("Please ernter some GOOD values!!!");
  }

}

function setAdd() {
  var add = document.getElementById("address").value;
  address = add;
  setMapByAdd();
}

function setMarker() {

  map.panTo(new google.maps.LatLng(latitude[count - 1], longitude[count - 1]));
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(latitude[count - 1], longitude[count - 1]),
      map: map});

}

function setMapByAdd() {
  geocoder = new google.maps.Geocoder();

  if (geocoder) {
    geocoder.geocode({
      'address': address
    }, function(result, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          var myLL = result[0].geometry.location;
          latitude[count] = myLL.lat();
          longitude[count] = myLL.lng();
          let newPoint = {
            latitude: latitude[count],
            longitude: longitude[count]
          };
          Points.push(newPoint);
          count++;
          setMarker();
        } else {
          alert("NO SUCH A PLACE!");
        }
      } else {
        alert("FAIL because:" + status);
      }
    });
  }
}

function main() {
  centerLL = new google.maps.LatLng(47.7510741, -120.7401385);
  myMap = {
    zoom: 4,
    center: centerLL,
    mapTypeID: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById('map'), myMap);

  document.getElementById("SearchLL").onclick = setLL;
  document.getElementById("SearchAdd").onclick = setAdd;

  document.getElementById("SendLL").onclick = function() {
    window.location.href = "LocationReceiver.php?latitude=" + latitude[count - 1] + "&longitude=" + longitude[count - 1];
  }
}

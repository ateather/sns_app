let express =  require('express')
let cookieParser = require('cookie-parser')
let bodyParser = require('body-parser')
let session = require('express-session')
let morgan = require('morgan')
let sha = require('sha.js')
let db = require('./database/db-ops')

const CLIENT_BASE = __dirname + "/client/"
const port = process.env.PROT || 8080
const app = express();
app.use(morgan('dev'))
app.set('trust proxy', 1)
app.use(cookieParser())
app.use(bodyParser())
app.use(session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}))

class User {
    constructor(email, pass, first_name, last_name, isPublisher) {
        this.email = email
        this.salt = "TempConstant"
        this.pass = sha('sha256').update(pass + this.salt).digest('hex')
        this.first_name = first_name
        this.last_name = last_name
        this.isPublisher = isPublisher
        this.hash = null
        this.hashTimeout = null
    }
}

// These arrays are virtualized DB tables, will be replaced later
let registeredUsers = []
let usersWaitingForValidation = []

// ---- DEBUG CODE ----
let debugUser = new User("m@meme.com", "pass", "firstname", "lastname", false)
registeredUsers[debugUser.email] = debugUser
// ---- END DEBUG ----

randomString = (len, charSet) => {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
}

app.use((req, res, next) => {
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie('user_sid');        
    }
    next();
});

// THIS IS JUST TEMPORARY ROUTING! 
// We will be using the vue router later
app.get('/', (req, res) => {
    res.redirect('/map')
})
app.get('/login', (req, res) => {
    res.sendFile(CLIENT_BASE + 'login.html')
})
app.get('/register', (req, res) => {
    res.sendFile(CLIENT_BASE + 'register.html')
})
app.get('/session', (req,res) => {
    let response = {
        session : req.session,
        cookie: req.cookies
    }
    res.send(JSON.stringify(response, null, 4))
    res.end()
})
app.get('/map', (req, res) => {
    let response = {
        session : req.session,
        cookie: req.cookies
    }
    console.log(JSON.stringify(response, null, 4))
    if (!(req.session.hash))
        res.redirect('/login')
    else 
        res.sendFile(CLIENT_BASE + 'map.html')
})
app.post('/mapdata', (req, res) => {
    res.sendStatus(200)
    res.end()
})



// TODO: Send session cookie 
let onLoginAttempt = (req, res) => {
    console.log(JSON.stringify(req.body, null, 4))
    let posted = new User(req.body.email, req.body.pass)
    db.tryLogin(req.body.email, posted.pass, yes => {
        let stored = yes //registeredUsers[req.body.email]
        //let posted = new User(req.body.email, req.body.pass)
        console.log("s: " + stored.password + " | p: " + posted.pass)
        if (stored.password == posted.pass) {
            req.session.hash = randomString(8)
            //registeredUsers[req.body.email].hash = req.session.hash // weird session stuff
            db.updateUserLocation(req.body.email, req.body.lat, req.body.long, result => {}) //update user location on login

            //res.redirect('/map')
            res.send({
                succ: true,
                isPublisher : stored.isPublisher
            })
        } else 
            res.send({m : "Invalid password provided"})
        
        res.end()
    }, no => {
        res.send({m : "Invalid email provided"})
        res.end()
    })
    
}

let onRegistrationAttempt = (req, res) => {
    console.log(JSON.stringify(req.body, null, 4))

    // A bit misleading in this case, but yes & no are variables
    //  passed to the lambda functions, not the function names
    db.doesUserExist(req.body.newEmail, result => {
        if (result) {
            res.send({m: "Email already registered"})
            res.end()
        } else {
            let isPublisher = req.body.isPublisher 
            let newUser = new User(req.body.newEmail, req.body.newPassword, req.body.newFirst, req.body.newLast, isPublisher)
            let AuthToken = randomString(8)
            usersWaitingForValidation[AuthToken] = newUser

            res.send({   
                m: "User must now validate registration",
                link: "/validate?t=" + AuthToken
            })
            res.end()
        }
    })
}

let onConfirmation = (req, res) => {
    if (req.query.t in usersWaitingForValidation) {
        let newUser = usersWaitingForValidation[req.query.t]

        db.insertUser(newUser, succ => {
            console.log(JSON.stringify(succ, null, 4))
            res.redirect('/login')
        }, fail => {
            // Could not write to the DB for some reason
            console.error(fail)
            res.redirect('/login')
        })
    } else {
        res.send({m: "token is invalid"})
        res.end()
    }
}
 // GET: /caregories?cid=basketball
 // POST: /categories    
 //       body: {}

let onGetAllCategories = (request, response) => {
    console.log(JSON.stringify(request.query, null, 4))
    let data = request.query

    db.getAllCategories(result =>{
        response.send(result)
        response.end()
    })
}

//getChildren - cat_name - [filled with cat_names]

let onGetChildCategories = (request, response) => {
    console.log(JSON.stringify(request.query, null, 4))
    let data = request.query
    let parent = data.parent == 'root' ? null : data.parent
    db.getImediateChildren(parent, result => {
        console.log("nnn")
        console.log(result)
        response.send( result)
        response.end()
    })
}

let onUpdatePreferences = (req, resp) => {
    console.log(JSON.stringify(req.body, null, 4))
    let data = req.body
    let callback = () =>{
        resp.send({m: "updated preferences "})
        resp.end()
    }

    if (data.value)
    {
        db.insertUserPreferences(data.email, data.cat_name, callback,callback)
    }
    else
    {
        db.deleteUserPreference(data.email, data.cat_name, callback)
    }

}

//getStory - title - story object

let onGetStory = (request, response) => {
    console.log(JSON.stringify(request.query, null, 4))
    let data = request.query

    db.getStory(data.title, request => {
        response.send(result)
        response.end()
    })
}

let onIsPublisher = (req, resp) => {
    console.log(JSON.stringify(req.body, null, 4))
    let data = req.body
    let callback = (result) =>{
        if(result)
        {
            resp.send({m: "yes: " + data.email + " is publisher"})
            resp.end()
        }
        else
        {
            resp.send({m: "no: " + data.email + " is not a publisher"})
            resp.end()
        }
    }

   db.isPublisher(data.email, callback)

}

let onPublishStory = (req, resp) => {
    console.log(JSON.stringify(req.body, null, 4))
    let data = req.body

    let callbackSucc = () =>{
        resp.send({
            succ: true,
            m: "success, new story added "
        })
        resp.end()
    }

    let callbackFail = () =>{
        resp.send({m: "fail, no new story added "})
        resp.end()
    }

//for new story if subOrNot == true: NewCategory is cname, dropdownCat is parent 
//false: dropdowncat is cname, parent is null

    let images =  JSON.stringify(data.image)


    if(data.subOrNot)
    {
        let cat_name = data.NewCategory
        if (data.extraCategories.length > 0)
            cat_name = cat_name.length > 0 ? cat_name + "," + data.extraCategories : data.extraCategories
        db.addNewStory(data.NewCategory, data.DropdownCategory, images, data.publisher, data.message, data.lat, data.long, data.range, data.date, data.title, callbackSucc, callbackFail)
    }
    else
    {
        //console.log(data.DropdownCategory.length <= 0 )
        let cat_name = data.DropdownCategory.length <= 0 ? data.NewCategory : data.DropdownCategory
        if (data.extraCategories.length > 0)
            cat_name = cat_name.length > 0 ? cat_name + "," + data.extraCategories : data.extraCategories
        db.addNewStory(cat_name, null, images, data.publisher, data.message, data.lat, data.long, data.range, data.date, data.title, callbackSucc, callbackFail) 
    }
}

// DEBUG: doesn't work, returns too early as db calls are async.
//          Must make promises for each getStory call
let onGetUserStories = (request, resp) => {
    console.log(JSON.stringify(request.query, null, 4))
    let data = request.query
    var toReturn = [];
    var current_date = new Date();

    db.getStoriesForUser(data.email, result => {
        let Promises = []
        console.log("HIT: " + result)
        for (var i = result.length - 1; i >= 0; i--) {
            let index = i
            let story = result[i]
            let p = new Promise((resolve, rj) => {
                db.getStory(story, result1 => {
                    if(result1.expiration_date < current_date){ //story is out of date, archive
                        db.deleteStory(result1.title, result3 =>{
                            if(result3)
                                resolve(null)
                            else
                                console.log("Could not delete out of date story, ERROR")
                        })
                    }
                    else{
                        resolve(result1)
                    }
                    //console.log(index + " / " + result.length)
                    //console.log("Pushing: " + result1)
                    //toReturn.push(result1)
                })
            })

            Promises.push(p)
        }

        Promise.all(Promises).then(values => {
            //console.log("Final gUS Return: " + values)
            values.forEach(story => {
                if(story != null)
                    toReturn.push(story)
            })
            resp.send(toReturn)
            resp.end()
        })
    })  
}

let onDeleteUser = (req, resp) => {
    console.log(JSON.stringify(req.body, null, 4))
    let data = req.body

    let callback = (result) =>{
        if(result)
        {
            resp.send({m: "yes: " + data.email + " was removed"})
            resp.end()
        }
        else
        {
            resp.send({m: "no: " + data.email + " could not be removed"})
            resp.end()
        }
    }

   db.deleteUser(data.email, callback)
}

let onDeleteStory = (req, resp) => {
    console.log(JSON.stringify(req.body, null, 4))
    let data = req.body

    let callback = (result) => {
        if(result)
        {
            resp.send({m: "yes story: " + data.title + " was removed"})
            resp.end()
        }
        else
        {
            resp.send({m: "no story: " + data.title + " could not be removed"})
            resp.end()
        }
    }

   db.deleteStory(data.title, callback)
}

let onUpdateLocation = (req, resp) => {
    console.log(JSON.stringify(req.body, null, 4))
    let data = req.body

    let callback = (result) => {
        if(result)
        {
            resp.send({m: "yes user: " + data.email + " was updated"})
            resp.end()
        }
        else
        {
            resp.send({m: "no, user: " + data.email + " was not updated"})
            resp.end()
        }
    }

    db.updateUserLocation(data.email, data.lat, data.long, callback)
}

let onGetPreferences = (req, response) => {
    //console.log("reee")
    console.log(JSON.stringify(req.query, null, 4))
    let data = req.query

    db.getUserPreference(data.email, result => {
        response.send(result)
        response.end()
    })
}

let onSearch = (req, res) => {
    console.log(JSON.stringify(req.body, null, 4))
    let data = req.body

    let Promises = []
    for (var i = data.categories.length - 1; i >= 0; i--) {
        var current = data.categories[i]
        //toReturn.push(current)

        let p = new Promise((resolve, reject) => {
            db.getCategoriesOfParent(current, result1 => {
                if(result1 != null)
                    resolve(result1) 
                else 
                    resolve([])
            })
        })

        Promises.push(p)
    }


    let expandedCategories = []
    Promise.all(Promises).then(values => {
        values.forEach(function(item){
            item.forEach(function(item2) {
                expandedCategories.push(item2)
            })
        });
        console.log("EC: " + expandedCategories)
    
        let lat = data.lat
        let long = data.long
        let storiesInRange = []
        db.getAllStories(stories =>{
            //console.log(JSON.stringify(result1, null, 4))
            let result1 = []
            for (var i in stories) {
                let current = stories[i]
                //console.log(JSON.stringify(current, null, 4))
                let titleCheck = new RegExp(data.title)
                let messageCheck = new RegExp(data.message)
                if (titleCheck.test(current.title)
                    && messageCheck.test(current.message_content))
                    result1.push(current)
            }

            for(var index in result1) {
                var current = result1[index]
                    var answerInMiles = db.distance(lat,long,current.lat,current.long, "M")
                    if(answerInMiles <= current.range){  //if distance is less than range
                        if(!storiesInRange.includes(current.title))  //patch to exclude duplicates 
                            storiesInRange.push(current.title)
                    }
                }
            

                // got stories in rand
                let ps = []
                storiesInRange.forEach(name => {
                    let p = new Promise((resolve, rj) => {
                        let new_name = name
                        db.getCategoriesForStory(new_name, result2 =>{
                            console.log("This is result for: " + new_name + " " + result2)
                            var value = db.intersect(expandedCategories, result2).length
                            console.log("Value: " + value)
                            if(value > 0){
                                //console.log("This is toReturn: " + new_name)
                                resolve(new_name)
                                
                            } else {
                                resolve('')
                            }
                        })
                    })
                    ps.push(p)
                })

                Promise.all(ps).then(values => {
                    console.log("BIG FINAL: " + values)
                    let result = []
                    values.forEach(title => {
                        if (title.length > 0)
                            result.push(title)
                    })
    
                    //cb(ret)
                    let Promises = []
                    console.log("HIT: " + result)
                    for (var i = result.length - 1; i >= 0; i--) {
                        let index = i
                        let story = result[i]
                        let p = new Promise((resolve, rj) => {
                            db.getStory(story, s => {
                                //console.log(index + " / " + result.length)
                                //console.log("Pushing: " + result1)
                                //toReturn.push(result1)
                                resolve(s)
                            })
                        })

                        Promises.push(p)
                    }

                    Promise.all(Promises).then(values => {
                        //console.log("Final gUS Return: " + values)
                        res.send(values)
                        res.end()
                    })
                })

        })

    })
}

// API Routes
app.post('/login', onLoginAttempt)
app.post('/register', onRegistrationAttempt)
app.get('/validate', onConfirmation)
app.get('/category', onGetAllCategories)
app.post('/preferences', onUpdatePreferences)
app.get('/preferences', onGetPreferences) // TODO: get SubbedTo categories
app.get('/children', onGetChildCategories) 
app.post('/story', onPublishStory)
app.get('/story', onGetStory)
app.post('/publisher', onIsPublisher)
//app.post('/newstory', onPublishStory)
app.get('/userstories', onGetUserStories) // DEBUG: see function
app.post('/delete', onDeleteUser)
app.post('/deleteStory', onDeleteStory)
app.post('/location', onUpdateLocation)
app.post('/search', onSearch)




// Server Startup
db.openConnection()
app.listen(port)
console.log('Listening to port: ' + port)

let res = {
    send : (obj) => {
        console.log("Responding with: ")
        console.log(JSON.stringify(obj, null, 4))
    },
    end : (obj) => {
        console.log("Ending Response")
    }
}



/*db.getUsersSubscribeCategories("Test1@me.com",result => {
    console.log(result)
})*/

/*db.getUserPreference("Test1@me.com", result =>{
    console.log(result)
})*/

/*.getStoriesInRangeForUser("Test1@me.com", result =>{
    console.log(result)
})*/

/*db.getStoryLocationDict(result => {
    console.log(JSON.stringify(result, null, 4))
})*/

/*db.getCategoriesForStory("Sports are Awesome! ", result =>{
    console.log("Sports are Awesome!:  ")
    console.log(result)
})*/

/*db.getCategoriesForStory("Interesting Piece ", result =>{
    console.log("Interesting Piece: ")
    console.log(result)
})*/

/*db.getAllStories(result => {
    console.log(result)
})*/

/*db.getStoriesForUser("Test1@me.com", result =>{
    console.log(result)
})*/

/*db.getImediateChildren("Baseball", result =>{
    console.log(result)
})*/

/*db.getStoriesInRangeForUser("Lauren@me.com",result => {
    console.log(result)
})*/

//Inactive preference, inactive story

/*db.getAllInactiveUsers(result => {
    console.log("Users: " + result)
})


db.getAllInactivePreferences(result => {
    console.log("Preferences: " + result)
})*/


/*db.getAllInactiveStories(result => {
    console.log("Stories: " + result)
})*/


/*db.getAllUsers(result => {
    console.log(result)
})

db.getAllUserPreferences(result =>{
    console.log(result)
})*/

/*db.getAllStories(result =>{
    console.log(result)
})*/

/*db.deleteStory("Lauren’s Story", result =>{
    console.log(result)
})*/


/*db.deleteUser("Testing@me.com", result => {
    console.log(result)
})*/




/*db.getStoriesForUser("Lauren@me.com", result => {
    console.log(result)
})*/

//onUpdatePreferences({body: {}}, res)








    





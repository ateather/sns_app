
CREATE TABLE People (
  email varchar(1024),
  password varchar(1024),
  salt varchar(1024),
  isPublisher int,
  PRIMARY KEY  (email)
);

CREATE TABLE Author (
  id int,
  name varchar(1024),
  PRIMARY KEY  (id)
);

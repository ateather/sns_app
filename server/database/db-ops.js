
let db = require('mongodb').MongoClient;
const mongoose = require('mongoose');
// Database Side Variables/Definitions 
const DB_URL = "mongodb://" + "0.0.0.0:23456"+ "/tunneldb";
const Schema = mongoose.Schema;

// User Table:  | email | password | salt | created_at | isPublisher | first_name | 
        //        last_name | last_activity
let UserSchema = new Schema({
    email: String,
    password: String,
    salt: String,
    created_at: Date,
    isPublisher: Boolean,
    first_name: String,
    last_name: String,
    last_activity: Date
});

let User = mongoose.model('User', UserSchema)

//InactiveUser Table:  | email | password | salt | created_at | isPublisher | first_name | last_name
let inactiveUserSchema = new Schema({
    email: String,
    password: String,
    salt: String,
    created_at: Date,
    isPublisher: Boolean,
    first_name: String,
    last_name: String
});

let InactiveUser = mongoose.model('InactiveUser', inactiveUserSchema)


//UserLocation Table: | email | lat | long |
let userLocationSchema = new Schema({
    email: String,
    lat: Number,
    long: Number
});

let UserLocation = mongoose.model('UserLocation', userLocationSchema)


//Active Preferences Table: category_id | email 

let activePreferenceSchema = new Schema({
    category_id: String,
    email: String
});

let ActivePreferences = mongoose.model('ActivePreferences', activePreferenceSchema)

//InActive Preferences Table: category_id | email 

let inactivePreferenceSchema = new Schema({
    category_id: String,
    email: String
});

let InactivePreferences = mongoose.model('InactivePreferences', inactivePreferenceSchema)

//Active Story Table: category_id | media | pub_email | message_content |
    //                location | range | expiration_date | tite | story_id 
let activeStorySchema = new Schema({
    category_id: String,
    media: String,
    pub_email: String,
    message_content: String,
    lat: Number,
    long: Number,
    range: Number,
    expiration_date: Date,
    title: String,
});

let ActiveStory = mongoose.model('ActiveStory', activeStorySchema)


//InActive Story Table: category_id | media | pub_email | message_content |
    //                location | range | expiration_date | tite | story_id 

let inactiveStorySchema = new Schema({
    category_id: String,
    media: String,
    pub_email: String,
    message_content: String,
    lat: Number,
    long: Number,
    range: Number,
    expiration_date: Date,
    title: String,
});



let InactiveStory = mongoose.model('InactiveStory', inactiveStorySchema)

// Category Table: category_id | cat_name | parent_id 

let categorySchema = new Schema({
    cat_name: String,
    parent_id: String
});

let Category = mongoose.model('Category', categorySchema)


let openConnection = () => {
    console.log(DB_URL);
    mongoose.connect(DB_URL, { useMongoClient: true });
    mongoose.Promise = global.Promise;
}

// boolean true if user is active user false elsewise 
let doesUserExist = (e, cb) => {
    console.log("Looking for User: " + e)
    User.where({email : e})
        .findOne((err, user) => {
            if (err || user == null) cb(false)
            else {
                console.log(user)
                cb(true)
            }
        })
}

// returns all publishers
let getAllPublishers = (cb) => {
    User.where({isPublisher: true})
        .find(null, (err, res) => {
            console.log(JSON.stringify(res, null, 4))
            cb(res)
        })
}

let getAllInactiveUsers = (cb) => {
    InactiveUser.find({}, (err, res) => {
        //console.log(JSON.stringify(res, null, 4))
        cb(res)
    });
}

let getAllInactivePreferences = (cb) => {
    InactivePreferences.find({}, (err, res) => {
        //console.log(JSON.stringify(res, null, 4))
        cb(res)
    });
}

let getAllInactiveStories = (cb) => {
    InactiveStory.find({}, (err, res) => {
        //console.log(JSON.stringify(res, null, 4))
        cb(res)
    });
}


//returns all active users
let getAllUsers = (cb) => {
    User.find({}, (err, res) => {
        cb(res)
    });
}


// boolean is user is a publisher
let isPublisher = (e, cb) => {
    User.where({
        email : e,
        isPublisher: true
    })
        .findOne((err,res) => {
            console.log(JSON.stringify(res,null,4))
            if (res == null) cb(false)
            else cb(true) 
        })
}

// insert new active user 
let insertUser = (user, succ, fail) => {
    console.log("Registering: " + user.email)
    new User({
        email: user.email,
        password: user.pass,
        salt: user.salt,
        isPublisher: user.isPublisher,
        created_at: new Date(),
        first_name: user.first_name,
        last_name: user.last_name
    }).save((err, obj) => {
        if (err) fail(err)
        else succ(obj)
    })
}

//attempt login
let tryLogin = (e, pass, succ, fail) => {
    User.where({email: e, password: pass})
        .findOne((err, user) => {
            if (err || user == null) fail(err)
            else {
                console.log(user)
                succ(user)
            }
        })
}


// does category exist boolean
let doesCategoryExist = (cname, cb) => {
    Category.where({
        cat_name : cname,
    })
        .findOne((err,res) => {
            console.log(JSON.stringify(res,null,4))
            if (res == null) cb(false)
            else cb(true) 
        })
}

// add new category with parent
let insertCategory = (name, parent_id, succ, fail) => {
    doesCategoryExist(parent_id, result => {
        if(!result && parent_id != null) {
            insertCategory(parent_id, null, succ => {  //if parent_id isnt a category it adds it
                    console.log("Parent: " + name + " did not exists in database")
                    console.log(JSON.stringify(succ, null, 4))
                }, fail => {
                // Could not write to the DB for some reason
                            console.error(fail)
                    });
        }
    })
	doesCategoryExist(name, result => {
    	if (!result) {
    		console.log("Adding New Category: " + name)
			new Category({
				parent_id: parent_id,
				cat_name: name,
			}).save((err,obj) => {
				if (err) fail(err)
				else succ(obj)
			})
    	}
    	else
    	{
    		console.log("Category: " + name + " already exists in database")
    	}
    })
}


// returns all categories
let getAllCategories = (cb) => {
    Category.find({}, (err, res) => {
        console.log(JSON.stringify(res, null, 4))
        cb(res)
    });
}


//removes a category from the list
let deleteCategory = (cname, cb) => {
    Category.deleteMany({ cat_name: cname }, function (err) {
        if (err) return handleError(err);
        cb(true)
   });
}


//gets all children of a parent category "node"
let getCategoriesOfParent = (node, cb) => {
        let childMaker = (cid, resolve, reject) => {
            let children = []
            //console.log("Finding: " + cid)
            Category.where({parent_id: cid}).find((e, r) => {
                if (r == null)
                {
                    //console.log("hit")
                    resolve(null)
                }
                else{
                let pending = []
                r.forEach(child => {
                    //console.log("hit child: " + child.cat_name + " for each")
                    children.push(child.cat_name)
                    let p = new Promise((rs, rj) => {
                        childMaker(child.cat_name,rs, rj)
                    })
                    pending.push(p)
                })
                Promise.all(pending).then((values) => {
                    let result = []
                    children.forEach(e => {
                        if(e.length > 0) result.push(e)
                    })
                    values.forEach(e => {
                        //console.log( e)
                        if (e.length > 0){
                            e.forEach(uhh => {
                                result.push(uhh)
                            })
                        } 
                    })
                    
                    resolve(result)
                })}
            })
        }

        new Promise((rs, rj) => {
            childMaker(node, rs, rj)
        }).then(cb)  
    }



// boolean if there is a preference for this user to this category
let doesPreferenceExist = (email,cname, cb) => {
    ActivePreferences.where({
        category_id: cname,
        email: email
    })
        .findOne((err,res) => {
            console.log(JSON.stringify(res,null,4))
            if (res == null) cb(false)
            else cb(true) 
        })
}


//adds new preference for a user and a category
let insertUserPreferences = (email, cat_name, succ, fail) => {
    doesUserExist(email, result => {
        if(result) {
            doesCategoryExist(cat_name, result => {
                if (result) {
                    doesPreferenceExist(email,cat_name, result => {
                    if (!result) {
                        console.log("Adding New User Preference: " + email + " adding " + cat_name)
                        new ActivePreferences({
                            category_id: cat_name,
                            email: email
                        }).save((err,obj) => {
                            if (err) fail(err)
                            else succ(obj)
                        })
                    }
                    else
                    {
                        console.log("User Preference: " + email + " , " + cat_name + " already exists in database")
                    }
                 })
                }
                else
                {
                    console.log("Category: " + cat_name + "does not exist")
                }
            })
        }
        else
        {
            console.log("User: " + email + "does not exist")
        }
    })
}


//removes preference and adds into archives preference
let deleteUserPreference = (email, cname, cb) => {
        ActivePreferences.deleteOne({ category_id: cname, email: email }, function (err) {
        if (err) return cb(false);
        cb(true)
        });    
}


//gets user object for email
let getUser = (email, cb) => {
    User.where({
        email : email,
    })
        .findOne((err,res) => {
            if (res == null) cb(null)
            else cb(res) 
        })
}

//removes user from active user list and adds to archive
let deleteUser = (email, cb) => {
    getUser(email, result =>{
        if(result == null) cb(false)
        else{
                insertInactiveUser(result, result1 =>{
                    if(result1) { //user added to archive
                        console.log("User Added to InActive Archive")

                        archivePreferencesForUser(email, result2 =>{
                            if(result2){ //preferences added to archive
                                removeActivePreferencesForUser(email, result3 =>{
                                    if(result3) { //preferences removed for user
                                        deleteUserFromActive(email, final_result =>{ //user deleted
                                            cb(final_result)
                                        })
                                    }
                                })
                                
                            }
                        })

                    }
                })
            cb(false)
            }
    })

    
}

let deleteUserFromActive = (email, cb) => {
    User.deleteMany({ email : email }, function (err) {
        if (err) return cb(false);
            cb(true);
    });
}

//adds user into archive
let insertInactiveUser = (user, cb) => {
    console.log("Adding user to Archive: " + user.email)
    new InactiveUser({
        email: user.email,
        password: user.pass,
        salt: user.salt,
        isPublisher: user.isPublisher,
        created_at: user.created_at,
        first_name: user.first_name,
        last_name: user.last_name
    }).save((err, obj) => {
        if (err) cb(false)
        else cb(true)
    })
}

//adds preference into archive

let archivePreferencesForUser = (email, cb) => {
    console.log("Adding User Preferences to Archive for: " + email)
    
    ActivePreferences.where({
        email: email
    })
    .find((err,res) => {
            if (res == null) {
                console.log("User had no preferences to Archive")
                cb(true)
            }
            else
            {
                for (var i = res.length - 1; i >= 0; i--) {
                    var current = res[i] 
                    var name = res[i].category_id   
                    new InactivePreferences({
                    category_id: name,
                    email: current.email
                    }).save((err,obj) => {
                    if (err) cb(false)
                    else console.log("Preference: " + obj.category_id + " removed")
                    }) 
                }

                cb(true)
            }
    })     
}

let removeActivePreferencesForUser = (email, cb) => {
    console.log("Deleting User Preferences for: " + email)
    
    ActivePreferences.deleteMany({ email: email }, function (err) {
        if (err) return cb(false);
        cb(true)
        });   
}

let insertInactiveStory = (story, cb) => {
    console.log("Adding Story to Archive: " + story.title)
        new InactiveStory({
                            category_id: story.category_id,
                            media: story.media,
                            pub_email: story.pub_email,
                            message_content: story.message_content,
                            lat: story.lat,
                            long: story.long,
                            range: story.range,
                            expiration_date: story.expiration_date,
                            title: story.title,
                        }).save((err,obj) => {
                            if (err) cb(false)
                            else cb(true)
                        })
}


//boolean is story exists for title
let doesStoryExist = (stitle, cb) => {
    ActiveStory.where({
        title : stitle,
    })
        .findOne((err,res) => {
            if (res == null) cb(false)
            else cb(true) 
        })
}

/*category_id: String,
    media: String,
    pub_email: String,
    message_content: String,
    lat: Number,
    long: Number,
    range: Number,
    expiration_date: Date,
    title: String,*/

let doesStoryExistTuple = (stitle, cat_name, slat, slong, date, cb) => {
    ActiveStory.where({
        title : stitle,
        category_id: cat_name,
        lat: slat,
        long: slong,
        expiration_date: date
    })
        .findOne((err,res) => {
            if (res == null) cb(false)
            else cb(true) 
        })
}

//This function assumes publisher is a valid publisher 
//adds a new story to the active story list 
let addNewStory = (cname, parent, media_link, publisher, smessage_content, slat, slong, srange, expiration_sdate, stitle, succ, fail) => {
    doesStoryExistTuple(stitle, cname, slat, slong, expiration_sdate, result => {
                    if (!result) {
                        let cats = cname.split(',')
                        let firstCategory = cats[0]
                        doesCategoryExist(firstCategory, result => {
                        if (!result) {
                            insertCategory(firstCategory, parent, succ => {
                                console.log(JSON.stringify(succ, null, 4))
                             }, fail => {
                                console.error(fail)
                             })
                        }})
                        console.log("Adding New Story: " + stitle)
                        new ActiveStory({
                            category_id: cname,
                            media: media_link,
                            pub_email: publisher,
                            message_content: smessage_content,
                            lat: slat,
                            long: slong,
                            range: srange,
                            expiration_date: expiration_sdate,
                            title: stitle,
                        }).save((err,obj) => {
                            if (err) fail(err)
                            else succ(obj)
                        })
                    }
                    else
                    {
                        let m = ("Story similar to: " + stitle + ", already exists in database")
                        fail(m)
                    }
                 })
}


//returns all active stories
let getAllStories = (cb) => {
    ActiveStory.find({}, (err, res) => {
        cb(res)
    });
}


//deletes story and adds into story archive 
let deleteStory = (stitle, cb) => {
    getStory(stitle, result1 => {
        insertInactiveStory(result1, result3 =>{
            if(result3) {
                console.log("Story to be removed was added to archive")
                ActiveStory.deleteMany({ title : stitle }, function (err) {
                    if (err) return cb(false);
                    cb(true)
                });
            } else {
                console.log("Story to be removed was not added to archive")
                cb(false)
            }
        })
    })
}


// gets active story for title
let getStory = (stitle, cb) => {
    ActiveStory.where({
        title : stitle,
    })
        .findOne((err,res) => {
            //console.log(JSON.stringify(res, null, 4))
            if (res == null) cb(res)
            else cb(res) 
        })
}


//gets category and children categories for given story title
let getCategoriesForStory = (stitle, cb) => {
    var toReturn = [];

    ActiveStory.where({
        title : stitle,
    })
        .findOne((err,res) => {
            if (res == null) cb(toReturn)
            else {
                toReturn = res.category_id.split(',')
                //toReturn.push(res.category_id)
                cb(toReturn)
                /*getCategoriesOfParent(res.category_id, result1 => {
                    toReturn.push(...result1)
                    cb(toReturn)
                })*/
            }
        })
}

//returns just the basic user preferences on API front_end
let getUserPreference  = (email, cb) => {
    var toReturn = [];
    ActivePreferences.where({
                email: email
            })
            .find((err,res) => {
            if (res == null) cb([]) //user has no preferences
            else {
                for (var i = res.length - 1; i >= 0; i--) {
                    toReturn.push(res[i].category_id)
                }

                cb(toReturn)
            } 
            })
}

//gets user preferences (returns what category a user is subscribed to)
let getUsersSubscribeCategories = (email, cb) => {
    let Promises = []
    var toReturn = []

    doesUserExist(email, result => {
        if(result){
            getUserPreference(email, res => {
                for (var i = res.length - 1; i >= 0; i--) {
                    var current = res[i]
                    //console.log("This is current: " + current)
                    toReturn.push(current)

                    let p = new Promise((resolve, reject) => {
                        getCategoriesOfParent(current, result1 =>{
                        if(result1 != null)
                        {
                           resolve(result1) 

                        } else resolve([])
                        
                        })
                    })

                    Promises.push(p)
                }



                Promise.all(Promises).then(values => {
                    //console.log("This is values: ")
                    //console.log(values)

                    values.forEach(function(item){
                        item.forEach(function(item2) {
                            //console.log(item2)
                            toReturn.push(item2)
                        })
                    });

                    //console.log("This is toReturn")
                    //console.log(toReturn)

                    cb(toReturn)

                })
            })
        }
        else
        {
            console.log("User: " + email + " is not a valid user")
            cb([])
        }
    })
}

//returns full list of user prefereces
let getAllUserPreferences = (cb) => {
    ActivePreferences.find({}, (err, res) => {
        //console.log(JSON.stringify(res, null, 4))
        cb(res)
    });
}


//updates users location 
let updateUserLocation = (email, lat, long, cb) => {
    doesUserLocationExist(email, result =>{
        if(result){
            UserLocation.updateOne(
           { email: email },
           {
             lat: lat,
             long: long
           },(err,obj) =>{
            if(err) cb(err)
                else cb(obj)
           })
        console.log("User: "+email+" location updated"  )
        }
        else
        {
            console.log("Saving new Location")
            new UserLocation({
                            email: email,
                            lat: lat,
                            long: long,
                        }).save((err,obj) => {
                            if (err) cb(err)
                            else cb(obj)
                        })
        }
    })
}

//boolean if use has a location
let doesUserLocationExist = (email, cb) => {
    UserLocation.where({
        email : email,
    })
        .findOne((err,res) => {
            //console.log(JSON.stringify(res,null,4))
            if (res == null) cb(false)
            else cb(true) 
        })
}

//returns full list of all user locations
let getAllUserLocations = (cb) => {
    UserLocation.find({}, (err, res) => {
        //console.log(JSON.stringify(res, null, 4))
        cb(res)
    });
}

//gets a single user's location
let getUserLocation = (email, cb) => {
    doesUserLocationExist(email, result => {
        if(result){
            UserLocation.findOne({email:email}, (err,res) =>{
                cb([res.lat,res.long])
            })
        }
        else{
            console.log("User: " + email + " does not have location")
            cb(null)
        }
    })
}

//gets the location of a story
let getStoryLocation = (title, cb) => {
    doesStoryExist(title, result => {
        if(result){
            ActiveStory.findOne({title:title}, (err,res) =>{
                cb([title,res.lat,res.long,res.range])
            })
        }
        else{
            console.log("Story: " + title + " does not exist")
            cb(null)
        }
    })
}


//function to find distance between two coordinates in miles
function distance(lat1, lon1, lat2, lon2, unit) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    }
    else {
        var radlat1 = Math.PI * lat1/180;
        var radlat2 = Math.PI * lat2/180;
        var theta = lon1-lon2;
        var radtheta = Math.PI * theta/180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180/Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit=="K") { dist = dist * 1.609344 }
        if (unit=="N") { dist = dist * 0.8684 }
        return dist;
    }
}

//returns full dictionary list of all stories locations
let getStoryLocationDict = (cb) =>{
    var dict = {};
    getAllStories(result2 =>{
                for (var i = result2.length - 1; i >= 0; i--) {
                    var current = result2[i]
                    //console.log(current)
                    
                    getStoryLocation(current.title, result3 =>{
                        dict[result3[0]] = result3;
                        cb(dict)
                    })
                }
            })
}


//returns full list of stories within mile range of user
let getStoriesInRangeForUser = (email, cb) =>{
    var storiesInRange = [];
    getUserLocation(email, result =>{
        if(result){
            let lat = result[0]
            let long = result[1]
            //cb([lat,long])
            getAllStories(result1 =>{
                //console.log(JSON.stringify(result1, null, 4))
                for(var index in result1) {
                    var current = result1[index]
                        var answerInMiles = distance(lat,long,current.lat,current.long, "M")
                        if(answerInMiles <= current.range){  //if distance is less than range
                            if(!storiesInRange.includes(current.title))  //patch to exclude duplicates 
                                storiesInRange.push(current.title)
                        }
                    }
                cb(storiesInRange)
            })
        }
        else
        {
            console.log("This user doesn't have location")
            cb([])
        }
    })
}

//function that returns the intersect of two arrays
function intersect(a, b) {
    var t;
    if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
    return a.filter(function (e) {
        return b.indexOf(e) > -1;
    });
}

//gets list of stories that are within a users range and what they subscribe to 
let getStoriesForUser = (email, cb) => {

var toReturn = [];
let Promises = []

    getUsersSubscribeCategories(email, result =>{
        console.log("gUSC: " + result)
        console.log("This is email: " + email)
        getStoriesInRangeForUser(email, result1 =>{
            console.log("This is result1: " + result1.length)
             for (var i = result1.length - 1; i >= 0; i--) {
                let name = result1[i]
                console.log("This is name: " + name)
                let p = new Promise((resolve, rj) => {
                    let new_name = name
                    getCategoriesForStory(new_name, result2 =>{
                        console.log("This is result for: " + new_name + " " + result2)
                        var value = intersect(result,result2).length
                        console.log("Value: " + value)
                        if(value > 0){
                            console.log("This is toReturn: " + new_name)
                            resolve(new_name)
                            
                        } else {
                            resolve('')
                        }
                    })
                })

                Promises.push(p)
             }

             Promise.all(Promises).then(values => {
                //console.log("BIG FINAL: " + values)
                let ret = []
                values.forEach(title => {
                    if (title.length > 0)
                        ret.push(title)
                })

                cb(ret)
                
            })


        })
    })
}

let getImediateChildren = (parent_id, cb) =>{  
    var toReturn = [];
    Category.where({
                parent_id: parent_id
            })
            .find((err,res) => {
            if (res == null) cb([]) //user has no preferences
            else {
                for (var i = res.length - 1; i >= 0; i--) {
                    toReturn.push(res[i].cat_name)
                }

                cb(toReturn)
            } 
        })

}

let removeOutOfDateStories = (current_date, cb) =>{
    var toReturn = [];

    ActiveStory.find({}, (err, res) => {
        if(err) cb(false)
        else{
            for (var i = res.length - 1; i >= 0; i--) {
                var current = res[i]

                if(current.date > current_date)
                {
                    var name = current.title
                deleteStory(current, result =>{
                    if(result)
                        console.log("Out of date story: " + name + " was removed")
                    else
                     console.log("Out of date story: " + name + " was not removed")
                })
                }
            }

            cb(true)
        }
        
    });
}








module.exports.openConnection = openConnection
module.exports.tryLogin = tryLogin

module.exports.doesUserExist = doesUserExist
module.exports.insertUser = insertUser
module.exports.getAllUsers = getAllUsers

module.exports.isPublisher = isPublisher
module.exports.getAllPublishers = getAllPublishers

module.exports.doesCategoryExist = doesCategoryExist
module.exports.insertCategory = insertCategory
module.exports.getAllCategories = getAllCategories
module.exports.deleteCategory = deleteCategory
module.exports.getCategoriesOfParent = getCategoriesOfParent

module.exports.doesPreferenceExist = doesPreferenceExist
module.exports.insertUserPreferences = insertUserPreferences
module.exports.deleteUserPreference = deleteUserPreference

module.exports.doesStoryExist = doesStoryExist
module.exports.addNewStory = addNewStory
module.exports.getAllStories = getAllStories
module.exports.deleteStory = deleteStory
module.exports.getStory = getStory

module.exports.getUsersSubscribeCategories = getUsersSubscribeCategories
module.exports.getAllUserPreferences = getAllUserPreferences
module.exports.updateUserLocation = updateUserLocation
module.exports.getAllUserLocations = getAllUserLocations
module.exports.doesUserLocationExist = doesUserLocationExist
module.exports.getUserLocation = getUserLocation
module.exports.getStoriesInRangeForUser = getStoriesInRangeForUser
module.exports.getStoriesForUser = getStoriesForUser
module.exports.getStoryLocation = getStoryLocation
module.exports.getStoryLocationDict = getStoryLocationDict
module.exports.getCategoriesForStory = getCategoriesForStory
module.exports.deleteUser = deleteUser
module.exports.insertInactiveUser = insertInactiveUser
module.exports.archivePreferencesForUser = archivePreferencesForUser
module.exports.removeActivePreferencesForUser = removeActivePreferencesForUser
module.exports.getUser = getUser
module.exports.insertInactiveStory = insertInactiveStory
module.exports.getUserPreference = getUserPreference
module.exports.getImediateChildren = getImediateChildren
module.exports.distance = distance
module.exports.intersect = intersect

module.exports.removeOutOfDateStories = removeOutOfDateStories
module.exports.doesStoryExistTuple = doesStoryExistTuple

module.exports.getAllInactiveUsers = getAllInactiveUsers
module.exports.getAllInactivePreferences = getAllInactivePreferences
module.exports.getAllInactiveStories = getAllInactiveStories
module.exports.deleteUserFromActive = deleteUserFromActive

